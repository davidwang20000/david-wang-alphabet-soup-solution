








const fs = require('fs');

function findWords() {
    const file = fs.readFileSync('input.txt', 'utf-8').trim().split('\n');

    const dimensions = file[0].split('x'); // Input is always given as (totalRows x columns) dimensions in the first line, so split by 'x' to get total rows and columns

    const formattedDimensions = dimensions.map((str) => Number(str)); // Create new formatted array with numbers

    const totalRows = formattedDimensions[0];
    const totalCols = formattedDimensions[1];

    const matrix = file.slice(1, 1 + totalRows)
                       .map(row => row.replace(/\r/g, '').split(' '))
                       .map(row => row.filter(char => char !== '')); // Filter out empty strings

    
    for(let i = totalRows + 1; i < file.length; i++) { 
        const word = file[i].trim();
        for(let row = 0; row < totalRows; row++){  
            for(let col = 0; col < totalCols; col++) { 
                if(matrix[row][col] == word[0]) { 
                    const directions = [[1, 1], [1, 0], [1, -1], [0, -1], [-1, -1], [-1, 0], [-1, 1], [0, 1]];

                    const maxRowLength = matrix.length;
                    const maxColumnLength = matrix[0].length;
                  
                    for(let direction of directions) {
                        let currentRow = 0;
                        let currentCol = 0;
                        let found = true; 
                  
                        for(let i = 0; i < word.length; i++) {
                            currentRow = (i * direction[0]) + row; 
                            currentCol = (i * direction[1]) + col;
                    
                            if (currentRow < 0 || currentRow >= maxRowLength || currentCol < 0 || currentCol >= maxColumnLength) { // Out of bounds check
                                found = false;
                                break;
                            }
                            if (matrix[currentRow][currentCol] !== word[i]) { // If current char does not match the char in the word, then break
                                found = false;
                                break;
                            }
                        }

                        if (found) {
                            console.log(word + ' ' + row + ':' + col + ' ' + currentRow + ':' + currentCol);
                        }
                    }  
                }
            }
        }
    }
}

findWords();